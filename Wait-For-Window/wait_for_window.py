import ctypes
import time
import subprocess
import sys

"""
Wait for an opened window to close and then execute a command.
This script is useful in large data copies. You subscribe a
watch on the copy window and when the copy is finished then
you can execute a showtdown command

Usage:
    $ wait_for_window.py [description] [sleep time] [cmd]
        [description]: part of the window title
        [sleep time]: time between each scan
        [cmd]: command to execute after the window closes

Example:
    wait_fot_window.py complete 5 "shutdown -s 0"

    (In Windows 10 during a copy the window title is something
    like "45% complete", therefore by using just the word
    complete the script will scan every 5 secs for the window
    and when copy is finished and the windows closes then
    executes a shutdown.)
"""

EnumWindows = ctypes.windll.user32.EnumWindows
EnumWindowsProc = ctypes.WINFUNCTYPE(ctypes.c_bool, ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_int))
GetWindowText = ctypes.windll.user32.GetWindowTextW
GetWindowTextLength = ctypes.windll.user32.GetWindowTextLengthW
IsWindowVisible = ctypes.windll.user32.IsWindowVisible

titles = []


def foreach_window(hwnd, lParam):
    """
    Get the window titles from all opened windows
    :param hwnd:
    :param lParam:
    :return:
    """
    if IsWindowVisible(hwnd):
        length = GetWindowTextLength(hwnd)
        buff = ctypes.create_unicode_buffer(length + 1)
        GetWindowText(hwnd, buff, length + 1)
        titles.append(buff.value)
    return True


def window_active(window_name):
    """
    Scan for partial windows_name in the windows titles list
    :param window_name: A part of the window name
    :return: True, if name if found in at least one title, otherwise False
    """
    # Clear list from previous entries
    titles.clear()
    EnumWindows(EnumWindowsProc(foreach_window), 0)
    for wnd_title in titles:
        if wnd_title.find(window_name) >= 0:
            return True
    return False


def wait_for_window(window_name, sleep_time, exec_cmd):
    """
    Wait for ever until the window closes
    :param window_name: The partial title name
    :param sleep_time: Sleep time in seconds after every scan
    :param exec_cmd: The shell command to execute
    :return: None
    """
    while window_active(window_name):
        time.sleep(sleep_time)
    subprocess.call([exec_cmd])


# Do not execute during import
if __name__ == "__main__":
    print('Scanning every %s sec for %s to terminate and execute: %s' % (sys.argv[2], sys.argv[1], sys.argv[3]))
    wait_for_window(sys.argv[1], int(sys.argv[2]), sys.argv[3])
