import requests
import json
import sys

""" Retrieves BF4 stats for a given player from http://api.bf4stats.com
    and prints the basic data of interest.

Version: 1.0

Usage (as standalone):
    bf4stats.py [player_name]
        [player_name]: string of the player name

Example (as standalone):
    bf4stats.py sgt_jaco

Result (as standalone):
    Name: sgt_jaco
    K/D: 0.87
    Accuracy: 0.12
    Headshots: 733 in 4407 kills (0.17)

Changelog:
    
"""


def GetBf4Stats(player_name):
    """ Retrieves BF4 stats for a given player
    Keyword arguments:
    player_name -- The name of the PC player to retrieve stats

    Returns: json dict object
    """
    userdata = {"plat": "pc", "name": player_name, "output": "json"}
    return requests.post('http://api.bf4stats.com/api/playerInfo', params=userdata).json()


def PrintBasicData(json_data):
    """ Prints basic data from the retrieved json

    Keyword arguments:
    json_data -- json dict object
    
    Returns: None
    """
    try:
        print('Name: %s' % json_data['player']['name'])
        print('K/D: %.2f' % (json_data['stats']['kills']/json_data['stats']['deaths']))
        print('Accuracy: %.2f' % (json_data['stats']['shotsHit']/json_data['stats']['shotsFired']))
        print('Headshots: %d in %d kills (%.2f)' % (json_data['stats']['headshots'], json_data['stats']['kills'], json_data['stats']['headshots']/json_data['stats']['kills']))
    except KeyError:
        print('Something went wrong.')
        print(json_data)

if __name__ == '__main__':
    PrintBasicData( GetBf4Stats(sys.argv[1]))
