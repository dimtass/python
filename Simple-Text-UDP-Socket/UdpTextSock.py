import socket
import re

"""
Create a simple UDP socket,  send data from a remote UDP port

Usage:
    $ UdpTextSock.py [mode] [local ip] [local port] [remote ip] [remote port] [data]
        [mode]: Data wait mode. 0: don't wai for response, 1: wait forever for response
        [local ip]: The local ip that the UDP socket will bind
        [local port]: The local port that the UDP socket will bind
        [remote ip]: The remote ip to send data
        [remote port]: The remote port to send data
        [data]: A string of data to send to remote UDP

Example:
    $ UdpTextSock.py 192.168.1.5 7710 192.168.1.147 55100 "This is a text"

"""

class UdpStrSock:
    def __init__(self, listen_ip, listen_port):
        """
        Create the listening UDP socket
        :param listen_ip: The listening IP address
        :param listen_port: The local listening port
        :return: None
        """
        self.listen_ip = listen_ip
        self.listen_port = listen_port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.bind((listen_ip, listen_port))
        self.remote_ip = None
        self.remote_port = None

    def setRemote(self, remote_ip, remote_port):
        """
        Set UDP socket's remote parameters
        :param remote_ip: The remote IP to send data
        :param remote_port: The remote port
        :return: True, if all ok, else False
        """
        # check if valid ip and port
        try:
            socket.inet_aton(remote_ip)
            self.remote_ip = remote_ip
            if 0 < remote_port < 65535:
                self.remote_port = remote_port
            else:
                self.remote_ip = None
                self.remote_port = None
                return False
        except socket.error:
            self.remote_ip = None
            self.remote_port = None
            return False

        return True

    def send(self, txt_data):
        """
        Send data without wait for responce
        :param txt_data: The string data to send
        :return: 0<=, if couldn't send data, else the length of the data sent
        """
        if self.remote_ip and self.remote_port:
            return self.sock.sendto(bytearray(str(txt_data), 'ascii'), (self.remote_ip, self.remote_port))
        return 0

    def sendRecv(self, txt_data):
        """
        Send data and wait for responce
        :param txt_data: The string data to send
        :return: 0<=, if couldn't send data, else the length of the data sent
        """
        if self.send(txt_data):
            return self.recv()

    def recv(self):
        """
        Wait forever to retrieve data in the listening port
        :return: string, of received data
        """
        while True:
            data, addr = self.sock.recvfrom(1024) # buffer size is 1024 bytes
            return data


# Do not execute during import
if __name__ == "__main__":
    import sys
    mode = int(sys.argv[1])
    lip = sys.argv[2]
    lport = int(sys.argv[3])
    rip = sys.argv[4]
    rport = int(sys.argv[5])
    udp_data = sys.argv[6]

    udp = UdpStrSock(lip, lport)
    udp.setRemote(rip, rport)
    if mode == 0:
        udp.send(udp_data)
    else:
        print(udp.sendRecv(udp_data))
