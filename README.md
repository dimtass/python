# python
This repo includes some random Python 3 scripts that I've written for personal use.
All scripts can be either used as a script file or module for importing classes and functions.
Available scripts:

  * BF4stats (v1.0 - 21.12.2015)
  * Batch file rename (v1.0 - 21.12.2015)
  * Simple UDP socket (v1.0 - 21.12.2015)
  * Find duplicate files (v1.0 - 21.12.2015)
  * 
