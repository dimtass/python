import exifread
import os
import collections
import argparse
import itertools

"""
Scans a path for image files and collects stats from EXIFs for Exposure time,
F-number, ISO speed ratings, Lens model, Camera model.
Then a result.csv file is created in the root path with all the stats.

If exiread module is not installed then install it with pip
    $ pip install exifread

Syntax:
    exif_stats.py -p PATH -x EXTENSION
        PATH: the path to search for image files
        EXTENSION: the extension of the images

Example:
    exif_stats.py "P:\Photos" cr2
"""

def dict_to_list(dictionary):
    l = []
    for key, value in dictionary:
        l.append([key, value])
    return l

def get_param(inp_list, index):
    ret = ['']
    try:
        ret = inp_list[index]
    except IndexError:
        pass
    return ret


def scan_exif_stats(folder, file_ext, exif):
    if exif is None:
        # if exif list is not declared then use the default EXIF params
        read_exif_params = ['EXIF FocalLength', 'EXIF FNumber', 'EXIF ISOSpeedRatings', 'EXIF LensModel', 'Image Model']
    else:
        read_exif_params = exif
    #create a list with collection objects for each exif parameter
    param_collections = []
    for k in read_exif_params:
        param_collections.append(collections.Counter())
    #reset photo counter
    num_of_photos = 0
    file_ext = '.' + file_ext
    # change dir to given path
    os.chdir(folder)
    #recursive search in given path for files with the given extension
    for root, dirs, files in os.walk('.'):
        # check for files in folder
        for file in files:
            # check file's extension
            if file_ext in file:
                num_of_photos += 1
                #read files and extract EXIF data
                f = open(os.path.join(root, file), 'rb')
                tags = exifread.process_file(f, details=False)
                for k, param in enumerate(read_exif_params):
                    try:
                        param_collections[k][str(tags[param])] += 1
                    except KeyError:
                        continue
                f.close()
    print('Found %d photos.' % num_of_photos)

    # create a list with converted dicts {a:b} to list [a, b]
    l = []
    for param in param_collections:
        l.append(dict_to_list(param.items()))
    # now l[] has a list with [key, value] lists
    # zip lists and if a list is shorter that an other then use
    # the maximum length and put None in the empty positions
    fl = list(itertools.zip_longest(*l))

    # create the CSV file
    csv = open('results.csv', 'w')
    csv.write('Photos found: ' + str(num_of_photos) + str('\n'))
    for i in read_exif_params:
        csv.write(i + str(',n,'))
    csv.write('\n')

    for each_param in fl:
        # print('it: ', each_param)
        for a in each_param:
            if a is None:   # if list item is None then change it to empty
                a = ['','']
            # print(str(a[0]) + ',' + str(a[1]), end=',')
            csv.write(str(a[0]) + ',' + str(a[1]) + ',')
        csv.write('\n')
        # print('\n')
    csv.close()
    print('results.csv created in %s' % folder)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Create a CSV file with EXIF stats in a given path')
    parser.add_argument('-p', '--path', nargs='+',
                       help='the path to search')
    parser.add_argument('-x', '--ext', nargs='+',
                       help='the image file extension')
    parser.add_argument('--exif', help='EXIF tags to use for stats')
    parser.add_argument('--print_tags', action='store_true', help='Prints supported EXIF tags')

    args = parser.parse_args()

    if args.print_tags == False:
        search_path = args.path[0]
        search_ext = args.ext[0]
        search_exif = args.exif
        if (search_path is not None) and (search_ext is not None):
            if os.path.exists(search_path):
                print('Starting search in %s for .%s' % (search_path, search_ext))
                scan_exif_stats(search_path, search_ext, search_exif)
            else:
                print('Path %s doesn\'t exists' % search_path)
        #scan_exif_stats(sys.argv[1], sys.argv[2])
    else:
        print('List of supported EXIF tags:')
        print('EXIF MaxApertureValue\n'
                'EXIF Flash\n'
                'EXIF FNumber\n'
                'Thumbnail Compression\n'
                'EXIF SubSecTimeDigitized\n'
                'JPEGThumbnail\n'
                'EXIF ShutterSpeedValue\n'
                'EXIF ExposureProgram\n'
                'Image Software\n'
                'EXIF FocalPlaneYResolution\n'
                'EXIF SceneCaptureType\n'
                'EXIF ExifVersion\n'
                'Image ResolutionUnit\n'
                'EXIF FocalPlaneXResolution\n'
                'Image YResolution\n'
                'EXIF DateTimeOriginal\n'
                'EXIF BodySerialNumber\n'
                'Thumbnail JPEGInterchangeFormat\n'
                'EXIF FocalLength\n'
                'Thumbnail JPEGInterchangeFormatLength\n'
                'Image DateTime\n'
                'Thumbnail YResolution\n'
                'EXIF MeteringMode\n'
                'Image XResolution\n'
                'EXIF DateTimeDigitized\n'
                'EXIF ExposureBiasValue\n'
                'Thumbnail ResolutionUnit\n'
                'Image ExifOffset\n'
                'Image Make\n'
                'EXIF FocalPlaneResolutionUnit\n'
                'EXIF ExposureMode\n'
                'EXIF ExposureTime\n'
                'EXIF ISOSpeedRatings\n'
                'EXIF CustomRendered\n'
                'EXIF LensSpecification\n'
                'Image Model\n'
                'EXIF ApertureValue\n'
                'EXIF ColorSpace\n'
                'EXIF LensModel\n'
                'EXIF WhiteBalance\n'
                'EXIF SubSecTimeOriginal\n'
                'Thumbnail XResolution\n')
