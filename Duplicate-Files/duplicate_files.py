import os
import sys
import glob
import hashlib
import itertools

"""
This script will scan the directory for duplicate files and will just list them.
If you want to add some functionality for the duplicate files then write your
code in the #your_code field

Usage:
    $ find_duplicate_files.py [path]
        [path]: the path to rearch for duplicate files

Example:
    find_duplicate_files.py .

"""


def list_duplicates(path_str):
    """
    Scan path for duplicate files
    :param path_str: The path to scan
    :return: None
    """
    if not os.path.exists(path_str):
        print("Path %s doesn't exists" % path_str)
        sys.exit(-1)

    os.chdir(path_str)
    list = [(fname, hashlib.md5(open(fname, 'rb').read()).digest()) for fname in glob.glob('*')]
    for a, b in itertools.combinations(list, 2):
        if a[1] == b[1]:
            print('%s is same with %s' % (a[0],b[0]))
            #your_code

# Do not execute during import
if __name__ == "__main__":
    pstr = sys.argv[1]
    list_duplicates(pstr)
