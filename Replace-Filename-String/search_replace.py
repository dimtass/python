import os
import sys
import glob
import hashlib
import itertools

"""
This script will scan the directory for duplicate files and will just list them.
If you want to add some functionality for the duplicate files then write your
code in the #your_code field

Usage:
    $ search_replace.py [path]
        [path]: the path to rearch for duplicate files

Example:
    $ search_replace.py /tmp/series/GOT s01e Game.of.Thrones.S01E
    (This will search the /tmp/series/GOT directory and will change all *s01e*
    files to Game.of.Thrones.S01E*)
"""

import glob
import os
import sys

pstr = sys.argv[1]
sstr = sys.argv[2]
rstr = sys.argv[3]

os.chdir(pstr)
files = glob.glob('*')
print(files)
for f in files:
    index = f.find(sstr)
    if index > 0:
        new = rstr + f[index+len(sstr):]
        print('%s -> %s' % (f, new))
        os.rename(f, new)

