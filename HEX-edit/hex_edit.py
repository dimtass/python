import os
import sys
import itertools
import re
import argparse

# Intel HEX regex
#   ^:([0-1][0-9A-Fa-f])([0-9A-Fa-f]{4,4})(0[0-4])([0-9A-Fa-f]+)([0-9A-Fa-f]{2,2})
#   ^:([0-9A-Fa-f]+)([0-9A-Fa-f]{2,2})

class HexTool:
    """
    Read Intel hex files, search for specific bytes and replace bytes chunks

    Syntax:
    hex_tool.py -[arguments]
    -i: input filename
    -o: ouput filename
    -s: search for bytes
    -r: replace bytes from a successive search
    -p: print hex file

    Usage:
    #Print HEX file
    hex_tool.py -i filename.hex -p

    #Search HEX file for specific bytes
    hex_tool.py -i filename.hex -s 012345AB

    #Replace specific bytes in HEX file and create a new HEX file (eg a MAC address)
    hex_tool.py -i filename.hex -s FCF8AE001023 -r F8DB88178921 -o new_filename.hex

    """
    def __init__(self, filename):
        if not os.path.isfile(filename):
            raise FileNotFoundError
        self.m_fname = filename
        self.m_hex, self.m_hex_lines = self.readHexFile(self.m_fname)
        self.found_cntr = 0
        self.pos = []
        if not self.m_hex:
            raise EOFError


    def readHexFile(self, fname):
        hex_data = []
        hex_lines = []
        file = open(fname, 'r')
        for line in file:
            hex_data += line.rstrip()
            hex_lines.append(line.rstrip())
        file.close()
        return hex_data, hex_lines


    def complement(self, number):
        return (~number + 1)%256


    def hexToBin(self, hex_str):
        return int(hex_str, 16)


    def printHEX(self):
        for line in self.m_hex_lines:
            print(line)


    def calculateChecksum(self, hex_bytes):
        csum = 0
        for i in range(0, len(hex_bytes), 2):
            csum += self.hexToBin(hex_bytes[i:i+2])
        return csum


    def verifyChecksum(self):
        for line in self.m_hex_lines:
            m = re.search('^:([0-9A-Fa-f]+)([0-9A-Fa-f]{2,2})', line)
            if m != None:
                hex_csum = self.hexToBin(m.group(2))
                csum = self.complement(self.calculateChecksum(m.group(1)))
                if hex_csum != csum:
                    print('False checksum %s != %s in line: %s' % (hex(hex_csum), hex(csum), line))
                    return False
            else:
                return False
        return True


    def recalculateChecksum(self):
        csum = 0
        hexsum = ''
        for i in range(0, len(self.m_hex)):
            if self.m_hex[i] == ':':
                # print('HEX sum of: ', hexsum[:-2], end=' : ')
                csum = self.complement(self.calculateChecksum(hexsum[:-2]))
                if i > 4:
                    # convert checksum to HEX string
                    str_csum = '{:02X}'.format(csum)
                    # replace checksum in HEX data
                    self.m_hex[i-2] = str_csum[0]
                    self.m_hex[i-1] = str_csum[1]
                # reset checksum for each line
                hexsum = ''
                csum = 0
            else:
                hexsum += self.m_hex[i]
                # print('hexsum: ', hexsum)


    def findInHex(self, find_bytes):
        self.found_cntr = 0
        self.pos.clear()
        pos = 0
        for line in self.m_hex_lines:
            m = re.search('^:([0-1][0-9A-Fa-f])([0-9A-Fa-f]{4,4})(0[0-5])([0-9A-Fa-f]+)([0-9A-Fa-f]{2,2})', line)
            try:
                if m != None:
                    data = m.group(4)
                    pos += 1 + len(m.group(1)+m.group(2)+m.group(3))
                    for i in range(0, len(data), 2):
                        hex_byte = data[i] + data[i+1]
                        # search mode
                        if hex_byte == (find_bytes[self.found_cntr]+find_bytes[self.found_cntr+1]):
                            self.found_cntr += 2
                            self.pos.append(pos)
                            if self.found_cntr == len(find_bytes):
                                return self.pos
                        else:
                            self.found_cntr = 0
                            self.pos.clear()
                        pos += 2
                    pos += len(m.group(5))
            except IndexError:
                return []
        return []


    def replaceInHex(self, find_bytes, replace_bytes):
        # Check if string exists in HEX file
        indexes = self.findInHex(find_bytes)
        if len(indexes) >= 0:
            print(indexes)
            for n, i in enumerate(indexes):
                print('changed: ', end=' ')
                print('%s' % (self.m_hex[i]+self.m_hex[i+1]), end=' ')
                print('to: ', end=' ')
                self.m_hex[i] = replace_bytes[n*2]
                self.m_hex[i+1] = replace_bytes[n*2+1]
                print('%s' % (self.m_hex[i]+self.m_hex[i+1]))
            self.recalculateChecksum()
        return False

    def saveHex(self, fname):
        of = open(fname, 'w')
        for ch in self.m_hex:
            if ch == ':':
                of.write('\n')
            of.write(ch)
        of.close()


if __name__ == "__main__":
    ohex = HexTool(sys.argv[1])
    ohex.printHEX()
    if ohex.verifyChecksum():
        print('HEX checksum ok')

    ohex.replaceInHex('3A28BF78C1FB', '010203040506')
    ohex.saveHex('new_firmware.hex')
